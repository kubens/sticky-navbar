/**
 * sticky 1.1.0
 * Add class 'stuck' when element reach top of browser.
 *
 * @author Jakub Laptas <kontakt@kubens.pl>
 * @link http://kubens.pl
 */

'use strict';

var bean = require('bean');
var addClass = require('dom101/add-class');
var removeClass = require('dom101/remove-class');
var hasClass = require('dom101/has-class');
var extend = require('dom101/extend');

/**
 * Default options for stickyNavbar
 * @type {Object}
 */
var defaults = {
	className: 'stuck',
	breakPoint: 768
};

var viewport = {
	/**
	 * Get viewport scrollY
	 * @return {integer}
	 */
	scrollY: function () {
		if (typeof window.scrollY === undefined) {
			return document.documentElement.scrollTop;
		}

		return window.scrollY;
	},

	/**
	 * Get viewport width
	 * @return {integer}
	 */
	width: function () {
		if (document.documentElement && document.documentElement.clientHeight) {
			return document.documentElement.clientWidth;
		}
		else if (document.body) {
			return document.body.clientWidth;
		}

		return 0;
	}
};

/**
 * Class constructor
 * @param {object} _element
 * @param {object} _options
 */
function StickyNavbar(_element, _options) {
	return this.init(_element, _options);
}

/**
 * Init stickyNavbar
 * @param  {object} _element element to check
 * @param  {object} _options
 * @return {object}
 */
StickyNavbar.prototype.init = function (_element, _options) {
	this.options = extend({}, defaults, _options);
	this.element = _element;
	this.navbarPos = _element.offsetTop;
	this.bindEvents();
};

/**
 * Check is width of browser is lower than break point width
 * @return {Boolean}
 */
StickyNavbar.prototype.isBreakpoint = function () {
	return this.options.breakPoint && viewport.width() < this.options.breakPoint;
};

/**
 * Check is element is on top of browser
 * @return {bool}
 */
StickyNavbar.prototype.check = function () {
	if (this.navbarPos <= viewport.scrollY()) {
		return this.stuckElement();
	}

	return this.unStuckElement();
};

/**
 * Refresh element position
 * @return {void}
 */
StickyNavbar.prototype.refresh = function () {
	removeClass(this.element, this.options.className);
	this.navbarPos = this.element.offsetTop;
	this.check();
};

/**
 * Add className to element
 * @return {void}
 */
StickyNavbar.prototype.stuckElement = function () {
	if (!hasClass(this.element, this.options.className)) {
		return addClass(this.element, this.options.className);
	}
};

/**
 * Remove className from element
 * @return {void}
 */
StickyNavbar.prototype.unStuckElement = function () {
	if (hasClass(this.element, this.options.className)) {
		removeClass(this.element, this.options.className);
	}
};

/**
 * Add listeners to window
 * @return {void}
 */
StickyNavbar.prototype.bindEvents = function () {
	var self = this;

	bean.on(window, 'load scroll touchmove', function () {
		if (self.isBreakpoint()) {
			return self.stuckElement();
		}

		self.check();
	});

	bean.on(window, 'resize', function () {
		if (self.isBreakpoint()) {
			return self.stuckElement();
		}

		self.refresh();
	});
};

module.exports = StickyNavbar;
