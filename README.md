# sticky-navbar

> Add class when navbar stuck on to of the viewport

* Author: [Jakub Laptas, skizze](http://skizze.pl)
* License: CC Attribution-NoDerivatives 4.0 International 
* Version: 1.1.0

TODO
---

1. Add unit test
2. Add jshint & jscs
3. Remove dom101 dependecies

CHANGELOG
---

#### 1.1.0

* added breakpoint option
* added className option
* changed to JavaScript class
* added JSDoc
* changed to tabs indent

#### 1.0.0

* initial version

Copyrights
-

© 2015 [Jakub Laptas](http://kubens.pl)
